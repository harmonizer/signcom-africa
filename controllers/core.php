<?php

session_start(); 

require_once('../models/lib/routing.php');
// define('DB', '');
// define('SITE', 'http://alias.dedawaltd.com/');
define('IMG', 'http://localhost:54928/Files/');

class Controller
{
    private $data, $route;

    public function __construct()
    {
        $this->route = Routing::checkRoute();
    }

    public function __destruct()
    {
        unset($this->data);
        unset($this->route);
        //unset($this);
    }

    public function Output()
    {
        switch($this->route)
        {
            case "home":
            case "about_us":
            case "contact_us":
            case "services":
            case "faq":
            {
                $this->getModelData(); // Process model via controllel (controls)
                break;
            }
            case "404":
            {
                $_GET['view'] = "404"; 
                break;
            }
            default:
            {
                header('location: 404');
                break;
            }
        }
        $this->generateView();  // Generate AND PROCESS VIEW
    }

    private function generateView()
    {
        $route = $_GET['view'];
        if($route == 'Login' || $route == 'Register' || $route == 'Forgot_Password'){
            require_once('../views/components/head.phtml'); // Get VIew
            require_once('../views/'.$route.'.phtml'); // Get VIew
        }else{
            require_once('../views/components/head.phtml'); // Get VIew
            require_once('../views/components/nav.phtml'); // Get VIew
            require_once('../views/'.$route.'.phtml'); // Get VIew
            require_once('../views/components/foot.phtml'); // Get VIew
        }
    }

    private function getModelData()
    {
        $route = $this->route;
        require_once('controls/'.$route.'.php'); // Get COntroller
        $this->data = $route::processController();
    }
}

try
{
    $controller = new Controller();
    $controller->Output();
    unset($controller);
}
catch(Exception $ex)
{
    echo $ex->getMessage();
}
?>