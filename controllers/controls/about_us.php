<?php

Abstract class about_us //extends DB_Model
{
    public function processController()
    {
        $count = (int)count(explode('/', $_GET['route'])); // COunt the url
        switch($count)
        {
            case 1:   // if url Dashboard
            {
                $_GET['title'] = "About - Signcom Africa";
                $_GET['description'] = "We are a <b>digital communications and technology</b> company that provides internal communications with digital signage, digital advertising with digital signage, social media management/marketing, and digital content creation services to Individuals and businesses of all sizes, principally in and around Accra, but also to companies throughout Ghana.";
                $_GET['page'] = "About Us";
                $_GET['view'] = "about";
                return array();
                break;
            }
            default:
            {
                throw new exception('Wrong url route porcess');
            }
        }
    }
}

?>