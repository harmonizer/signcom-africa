<?php

Abstract class home //extends DB_Model
{
    public function processController()
    {
        $count = (int)count(explode('/', $_GET['route'])); // COunt the url
        switch($count)
        {
            case 1:   // if url Dashboard
            {
                $_GET['title'] = "Home - Signcom Africa";
                $_GET['description'] = "Digital Signage is a network of customizable displays that can be controlled electronically using a computer, allowing content to be changed remotely for the most targeted messaging possible. Digital signage is used for a wide variety of purposes including customer facing and employee-facing applications such as advertising, enhancing customer or employee experiences, influencing audience behavior, brand building, entertainment, security, interactive kiosks, creating corporate communities, etc.";
                $_GET['page'] = "Home";
                $_GET['view'] = "home";
                return array();
                break;
            }
            default:
            {
                throw new exception('Wrong url route porcess');
            }
        }
    }
}

?>