<?php

Abstract class Routing
{
    private static $routes = array("home","about_us","contact_us","services","faq");

    public static function checkRoute()
    {
        if(empty($_GET['route']))
        {
            $_GET['route'] = "home";
        }
        $_GET['title'] = explode('/', $_GET['route'])[0];
        return (in_array($_GET['title'], self::$routes)) 
                ? $_GET['title'] 
                : "404";
    }
}

?>