$(document).ready(function() {

    var pathArray = window.location.pathname.split( '/' ); var i;
    for (i = 0; i < pathArray.length; i++) {
      if(pathArray[i] !== ""){ $('.'+pathArray[i]).addClass('lazur-bg'); }
    }

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    $('#Back').click(function(){
        window.location.reload();
    });
    $('.Back').click(function(){
        window.location.reload();
    });

    $('#Add').click(function(){
        $('.Add').addClass('hidden');
        $('#Back').removeClass('hidden');
        $('.View').addClass('hidden');
        $('.AddView').removeClass('hidden');
    });

    var tour = new Tour({
        steps: [{
                element: "#step1",
                title: "Account Summary",
                content: "This shows the total number of Staff, Customer, Message that exist for current user.",
                placement: "bottom",
                backdrop: true,
                backdropContainer: '#wrapper',
                onShown: function (tour){
                    $('body').addClass('tour-open')
                },
                onHidden: function (tour){
                    $('body').removeClass('tour-close')
                }
            },
            {
                element: "#step2",
                title: "User Profile",
                content: "This is a brief Summary of the current User/Company Profile.",
                placement: "top",
                backdrop: true,
                backdropContainer: '#wrapper',
                onShown: function (tour){
                    $('body').addClass('tour-open')
                },
                onHidden: function (tour){
                    $('body').removeClass('tour-close')
                }
            },
            {
                element: "#step3",
                title: "Change Details",
                content: "Allow you to Change your SenderId, Profile Image and Password.",
                placement: "top",
                backdrop: true,
                backdropContainer: '#wrapper',
                onShown: function (tour){
                    $('body').addClass('tour-open')
                },
                onHidden: function (tour){
                    $('body').removeClass('tour-close')
                }
            },
            {
                element: "#step4",
                title: "Monthly SMS Report",
                content: "Gives You the Monthly Report on sms sent and sms schedule to be sent.",
                placement: "top",
                backdrop: true,
                backdropContainer: '#wrapper',
                onShown: function (tour){
                    $('body').addClass('tour-open')
                },
                onHidden: function (tour){
                    $('body').removeClass('tour-close')
                }
            },
            {
                element: "#step5",
                title: "Staff Manager",
                content: "This is where you can Add, Edit, Update and Delete any staff Details from here.",
                placement: "bottom"
            },
            {
                element: "#step6",
                title: "Customer Manager",
                content: "This is where you can Add, Edit, Update and Delete any staff Customer from here.",
                placement: "bottom"
            },
            {
                element: "#step7",
                title: "ALert - Sms Broadcast",
                content: "Where you can send, schedule and Set Automated Messages to be send to Staff or Customer.",
                placement: "bottom"
            },
            {
                element: "#step8",
                title: "Messages Log",
                content: "Shows you all list of Automated Birthday and Aniversary messages sent and gives the option to Resend.",
                placement: "bottom"
            },
            {
                element: "#step9",
                title: "SmsLog",
                content: "Shows you all list of scheduled and Broadcast messages sent and gives the option to Resend.",
                placement: "bottom"
            },
            {
                element: "#step10",
                title: "Sms Credit Balance",
                content: "This tells you the no of sms units you have left, i.e the no of Sms you cana send.",
                placement: "bottom"
            },
            {
                element: "#step11",
                title: "User Logout",
                content: "Destory your current session and Exit the Application.",
                placement: "bottom"
            },
            {
                element: "#step12",
                title: "User Profile",
                content: "Shows Your User Details so you can Edit and Update it..",
                placement: "bottom"
            },
            {
                element: "#step13",
                title: "Company Name",
                content: "Shows your Company Name.",
                placement: "top",
                backdrop: true,
                backdropContainer: '#wrapper',
                onShown: function (tour){
                    $('body').addClass('tour-open')
                },
                onHidden: function (tour){
                    $('body').removeClass('tour-close')
                }
            }
        ]});

        // Initialize the tour
        //tour.init();

        $('.startTour').click(function(){
            tour.restart();

            // Start the tour
            // tour.start();
        })

    $(document).ready(function () {
        // Set idle time
        $( document ).idleTimer(20000);
    });

    $( document ).on( "idle.idleTimer", function(event, elem, obj){
        toastr.options = {
            "closeButton": true,
            "preventDuplicates": false,
            "positionClass": "toast-top-right",
            "timeOut": 10000,
            "progressbar": true,
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        toastr.warning('you are idle for 20 minutes u will be Disconnected from server in 10second','Connection Deactivated');
    });

    $( document ).on( "active.idleTimer", function(event, elem, obj, triggerevent){
        // function you want to fire when the user becomes active again
        toastr.clear();
        toastr.success('Welcome Back User ','Connection Active');
    });
});
