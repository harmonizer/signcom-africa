

var Urls = atob(sessionStorage.getItem('ip'))+ 'api', Img = atob(sessionStorage.getItem('ip'))+'Files/'
var date = atob(sessionStorage.getItem('date')), cityId = atob(sessionStorage.getItem('countryId')); 
var userId = atob(sessionStorage.getItem('userId')), limit = 4; //console.log(cityId);
var auth = atob(sessionStorage.getItem('auth')); 
    headers = {"Authorization": "Bearer " + auth };

    var myApp = angular.module('myApp', []/*['ngAnimate']*/);

        myApp.run(function($rootScope) {
            $rootScope.date = new Date(date);
            $rootScope.ImageUrl = Img;
        });

        myApp.filter('capitalize', function() {
            return function(input, all) {
              var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
              return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
            }
        });
        
        // myApp.filter('countdown', [function() {
        //     return function(seconds) {
        //         return new Date(1970, 0, 1).setSeconds(seconds);
        //     };
        // }])
        // myApp.filter('countdowns', function() {
        //     function padTime(t) {
        //         return t < 10 ? "0"+t : t;
        //     }

        //     return function(_seconds) {
        //         if (typeof _seconds !== "number" || _seconds < 0)
        //             return "00:00:00";

        //         var hours = Math.floor(_seconds / 3600),
        //             minutes = Math.floor((_seconds % 3600) / 60),
        //             seconds = Math.floor(_seconds % 60);

        //         return padTime(hours) + ":" + padTime(minutes) + ":" + padTime(seconds);
        //     };
        // });

        myApp.controller('boardcontroller', function($scope, $http) {
            $http.get(Urls+"/App/Dash/"+userId, {headers: headers}).then(function (response) {
                $scope.dashs = response.data;
            });
        });

        myApp.controller('countrycontroller', function($scope, $http) {
            auth = getToken(); headers = {"Authorization": "Bearer " + auth, 'Access-Control-Allow-Origin' : '*'};
            $http.get(Urls+"/AppCountry/", {headers: headers}).then(function (response) {
                $scope.country = response.data;
            });
        });

        myApp.controller('usercontroller', function($scope, $http) {
            // if(sessionStorage.getItem('userId') == null){ sessionStorage.clear(); window.location.href = "login"}
            $http.get(Urls+"/AppUsersDetails/User/"+userId, {headers: headers}).then(function (response) {
                $scope.dash = response.data; var w = $scope.dash.AppUser.SmsCredit; $scope.Imgurl= Img+'User/'; 
                $scope.dash.AppUser.SmsCredit = w.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                $scope.chp = {}; $scope.chp.Username = $scope.dash.AppUser.Username;
             }, function(response) { 
                toastr.warning(response.statusText+': '+response.data+' '+'Not Successfull','User Details');
                // checkStatus(response.status); 
            });
            $scope.changepassword = function() {
                $http.post( Urls+"/App/Changepwd/",JSON.stringify($scope.chp),{headers: headers}).then(function(response) {
                    $scope.rest = response.data; 
                    $scope.chp.OldPassword = ''; $scope.chp.NewPassword = ''; $('.Close').click();
                    displayMsg("success", "Changed Successfull", "Password Has Been Changed Successfully");
                }, function(response) { 
                    toastr.warning(response.statusText+': '+response.data+' '+'Not Successfull','Change Password');
                });
            };
            $scope.upload = function() {
                var pass = $('#Passport').get(0).files; var ptype = 'User';
                $scope.up={AppUserId: userId}; $scope.up.ProfileImage = fileUpload(ptype, pass);
                $http.post( Urls+"/App/Profile/",JSON.stringify($scope.up),{headers: headers}).then(function(response) {
                    $scope.rest = response.data;  
                    $scope.dash.AppUser.Image = $scope.up.ProfileImage; $('.Close').click();
                    displayMsg("success", "Changed Successfull", "profile Image Has Been Changed Successfully");
                }, function(response) { 
                    toastr.warning(response.statusText+': '+response.data+' '+'Not Successfull','Upload Profile Image');
                });
            };
            $scope.logout = function(user) {
                $http.get( Urls+"/Out/"+user,{headers: headers}).then(function(response){
                    $scope.reg = response.data; 
                    displayMsg("success", 'logOut Successfull', 'You have been Logout Successfully');
                    sessionStorage.clear(); window.location.href = "login";
                }, function(response) { 
                    toastr.warning(response.statusText+': '+response.data+' '+'Not Successfull','LogOut');
                    sessionStorage.clear(); window.location.href = "login";
                });
            };
        });

        myApp.controller('logincontroller', function($scope, $http) {
            $scope.login = function() {
                auth = getToken(); headers = {"Authorization": "Bearer " + auth, 'Access-Control-Allow-Origin' : '*'};
                $http.post( Urls+"/App/Access/",JSON.stringify($scope.log),{headers: headers}).then(function(response) {
                    $scope.reg = response.data; //console.log(response); console.log($scope.reg);
                    displayMsg("success", "Access Granted", "Welcome "+ $scope.reg.User.Details[0].FullName + "  login Date: " + $scope.reg.Date);
                    sessionStorage.setItem('userId', btoa($scope.reg.User.AppUserId));
                    sessionStorage.setItem('auth', btoa($scope.reg.Access_Token));
                    sessionStorage.setItem('date', btoa($scope.reg.Date));
                    sessionStorage.setItem('countryId', btoa($scope.reg.User.Details[0].CountryId));
                    setInterval(function(){ window.location.href = "dashboard" }, 1000);
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
            $scope.forgetpassword = function() {
                auth = getToken(); headers = {"Authorization": "Bearer " + auth, 'Access-Control-Allow-Origin' : '*'};
                $http.post( Urls+"/App/UserReset/",JSON.stringify($scope.log),{headers: headers}).then(function(response) {
                    $scope.reg = response.data; console.log(response); 
                    displayMsg("success", "Reset Successfull", "Check Your Mail For Confirmation ");
                    setInterval(function(){ window.location.href = "login" }, 1000);
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
            $scope.register = function() {
                auth = getToken(); headers = {"Authorization": "Bearer " + auth, 'Access-Control-Allow-Origin' : '*'};
                var pass = $('#Logo').get(0).files; var ptype = 'Logo', stype = 'User';
                $scope.regis.Detail.Logo = fileUpload(ptype, pass); //console.log($scope.regis.Detail.Logo);
                $scope.regis.AppUser.Image = fileUpload(stype, pass); //console.log($scope.regis.AppUser.Image);
                //$scope.regis.Detail.Logo = $scope.regis.AppUser.Image;
                $http.post( Urls+"/App/Register/",JSON.stringify($scope.regis),{headers: headers}).then(function(response) {
                    $scope.reg = response.data; console.log(response);
                    displayMsg('success', 'Register', $scope.reg.Output); 
                    setInterval(function(){ window.location.href = "login"}, 1000);
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
        });

        myApp.controller('profilecontroller', function($scope, $http) {
            $http.get(Urls+"/AppCountry/", {headers: headers}).then(function (response) {
                $scope.countrys = response.data;
             }, function(response) { 
                toastr.warning(response.statusText+': '+response.data+' '+'Not Successfull','Country Details');
                checkStatus(response.status); 
            });
            $http.get(Urls+"/App/Dash/"+userId, {headers: headers}).then(function (response) {
                $scope.dashs = response.data;
             }, function(response) { 
                toastr.warning(response.statusText+': '+response.data+' '+'Not Successfull','User Details');
                checkStatus(response.status); 
            });
            $http.get(Urls+"/AppUsersDetails/User/"+userId, {headers: headers}).then(function (response) {
                $scope.dash = response.data; $scope.dash.DateOfInC = new Date($scope.dash.DateOfInC); 
                var w = $scope.dash.AppUser.SmsCredit; $scope.Imgurl= Img+'Logo/'; 
                $scope.dash.AppUser.SmsCredit = w.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
             }, function(response) { 
                toastr.warning(response.statusText+': '+response.data+' '+'Not Successfull','User Details');
                checkStatus(response.status); 
            });
            $http.get(Urls+"/AppSchedules/User/"+userId, {headers: headers}).then(function (response) {
                $scope.sches = response.data;
             }, function(response) { 
                toastr.warning(response.statusText+': '+response.data+' '+'Not Successfull','User Details');
                checkStatus(response.status); 
            });
            $http.get(Urls+"/AppMessages/User/"+userId, {headers: headers}).then(function (response) {
                $scope.msg = response.data;
              }, function(response) { 
                toastr.warning(response.statusText+': '+response.data+' '+'Not Successfull','User Details');
                checkStatus(response.status); 
            }); 
            $scope.count = limit; $scope.sug = { AppUserId: userId, CountryId: cityId}
            $scope.load = function() {
                $scope.page++;
                $scope.count = limit * $scope.page; 
            };
            $scope.update = function(id) {
                $http.put( Urls+"/AppUsersDetails/"+id, JSON.stringify($scope.dash), {headers: headers}).then( function(response){
                    $scope.resp = response.data; $scope.Edit = false; //console.log(response);
                    displayMsg('success', 'Successfully', "Profile Updated Successfully");
                 }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
            $scope.suggest = function() {
                $http.post( Urls+"/Suggestions/", JSON.stringify($scope.sug), {headers: headers}).then( function(response){
                    $scope.sug = response.data; $scope.sug = { AppUserId: userId, CountryId: cityId}
                    // displayMsg('success', 'Successfully', "Profile Updated Successfully");
                    toastr.success('Suggestion Sent, we will go through your request and make reasonable changes','Sent'); 
                 }, function(response) {
                    toastr.warning(response.statusText+': '+response.data+' '+'Not Successfull','Not Successfull'); 
                });
            };
        });

        myApp.controller('staffcontroller', function($scope, $http) {
            $http.get(Urls+"/Staffs/User/"+userId, {headers: headers}).then(function (response) {
                $scope.staffs = response.data;
            }, function(response) { 
                displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                checkStatus(response.status);
            });
            $scope.Imgurl = Img+'Bday/'; $scope.count = limit;  
            $scope.staff = {AppUserId: userId, CountryId: cityId};
            $scope.load = function() {
                $scope.page++;
                $scope.count = limit * $scope.page; 
            };
            $scope.postStaff = function() {
                $scope.staff.CountryId = cityId; var pass = $('#Image').get(0).files; var ptype = 'Bday'; 
                $scope.staff.Image = fileUpload(ptype, pass); console.log($scope.staff.Image);
                $http.post( Urls+"/Staffs/",JSON.stringify($scope.staff),{headers: headers}).then(function (response) {
                    $scope.staff = response.data; $scope.staff.DateOfBirth = new Date($scope.staff.DateOfBirth);
                    if($scope.staff.DateOfWedding != null){ $scope.staff.DateOfWedding = new Date($scope.staff.DateOfWedding);} 
                    $scope.staffs.push($scope.staff); $scope.staff = {AppUserId: userId, CountryId: cityId};
                    displayMsg('success', 'Created', "Staff Added Successfully");
                    $scope.hideform = false; $scope.add = false;
                }, function(response) {
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
            $scope.editStaff = function(id) {
                $scope.hideform = true; $scope.add = true;
                if (id == 'new') {
                    $scope.edit = false;
                    $scope.staff = {AppUserId: userId};
                }else{
                    $scope.edit = true;
                    angular.forEach($scope.staffs, function (value, key) {
                        if(value.StaffId == id){                            
                            value.DateOfBirth = new Date(value.DateOfBirth);
                            value.DateOfWedding = new Date(value.DateOfWedding);
                            $scope.staff = value;
                        }
                    });
                }
            };
            $scope.updateStaff = function(id) {
                var pass = $('#Image').get(0).files; var ptype = 'Bday';
                var pload = fileUpload(ptype, pass); console.log(pload);
                if(pload.toLowerCase() != 'file not selected') {$scope.staff.Image = pload}
                $http.put( Urls+"/Staffs/"+id, JSON.stringify($scope.staff) ,{headers: headers}).then(function (response) {
                    $scope.tod = response.data; 
                    displayMsg('success', 'Updated', "Staff Updated Successfully");
                    setInterval(function(){ window.location.reload() }, 2000);
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
            $scope.deleteStaff = function(id) {
                $http.delete( Urls+"/Staffs/"+id, {headers: headers}).then(function (response) {
                    $scope.tod = response.data; 
                    displayMsg('success', 'Deleted', 'Staff Deleted Successfully');
                    setInterval(function(){ window.location.reload() }, 2000);
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
        });

        myApp.controller('customercontroller', function($scope, $http) {
            $http.get(Urls+"/AppCustomers/User/"+userId, {headers: headers}).then(function (response) {
                $scope.custs = response.data;
            }, function(response) { 
                displayMsg('warning', 'Not Successfull', response.statusText);
                checkStatus(response.status);
            }); 
            $scope.cust = {AppUserId: userId, CountryId: cityId }; $scope.count = limit;
            $scope.load = function() {
                $scope.page++;
                $scope.count = limit * $scope.page; 
            };
            $scope.postCust = function() {
                $scope.cust.CountryId = cityId;
                $http.post( Urls+"/AppCustomers/",JSON.stringify($scope.cust),{headers: headers}).then(function (response) {
                    $scope.cust = response.data; $scope.cust.DateOfBirth = new Date($scope.cust.DateOfBirth);
                    $scope.custs.push($scope.cust); $scope.cust = {AppUserId: userId, CountryId: cityId}; 
                    displayMsg('success', 'Created', "Customer Added Successfully"); 
                    $scope.hideform = false; $scope.add = false;
                    // setInterval(function(){ window.location.reload() }, 2000);
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
            $scope.editCust = function(id) {
                $scope.hideform = true; $scope.add = true;
                if (id == 'new') {
                    $scope.edit = false;
                    $scope.cust = {AppUserId: userId};
                }else{
                    $scope.edit = true;
                    angular.forEach($scope.custs, function (value, key) {
                        if(value.CustomerId == id){                            
                            value.DateOfBirth = new Date(value.DateOfBirth);
                            $scope.cust = value;
                        }
                    });
                }
            };
            $scope.updateCust = function(id) {
                $http.put( Urls+"/AppCustomers/"+id,JSON.stringify($scope.cust),{headers: headers}).then(function (response){
                    $scope.tod = response.data; 
                    displayMsg('success', 'Updated', "Customer Updated Successfully");
                    setInterval(function(){ window.location.reload() }, 2000);
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText);
                });
            };
            $scope.deleteCust = function(id) {
                $http.delete( Urls+"/AppCustomers/"+id, {headers: headers}).then(function (response) {
                    $scope.tod = response.data; 
                    displayMsg('success', 'Deleted', 'Customer Deleted Successfully');
                    setInterval(function(){ window.location.reload() }, 2000);
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText);
                });
            };
        });

        myApp.controller('alertcontroller', function($scope, $http) {
            $scope.send = {AppUserId: userId, SenderId: "Acyst", CountryId: cityId};
            $scope.sche = {AppUserId: userId, CountryId: cityId};
            $scope.sing = {AppUserId: userId, CountryId: cityId};
            $scope.save = {AppUserId: userId, CountryId: cityId, Status: true, Name: false}; 
            $scope.alert = {Option: "false"};
            $http.get(Urls+"/Staffs/User/"+userId, {headers: headers}).then(function (response) {
                $scope.staffs = response.data;
                }, function(response) { 
                displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                checkStatus(response.status);
            });
            $http.get(Urls+"/AppCustomers/User/"+userId, {headers: headers}).then(function (response) {
                $scope.custs = response.data;
                }, function(response) { 
                displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                checkStatus(response.status);
            }); 
            $http.get(Urls+"/AppSchedules/User/"+userId, {headers: headers}).then(function (response) {
                $scope.sches = response.data;
                }, function(response) { 
                displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                checkStatus(response.status);
            }); 
            $scope.broadcast = function() {
                $scope.send.Page = $scope.send.SmsPrice;
                $http.post( Urls+"/AppSms/",JSON.stringify($scope.send),{headers: headers}).then(function(response){
                    $scope.reg = response.data; $scope.send = {AppUserId: userId, CountryId: cityId};
                    displayMsg('success', 'Successfull', $scope.reg.Response);
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
            $scope.schedule = function() {
                $scope.sche.Page = $scope.sche.SmsPrice; $scope.sche.Status = "Pending";
                $http.post( Urls+"/AppSchedules/",JSON.stringify($scope.sche),{headers: headers}).then(function(response){
                    $scope.sche = response.data; $scope.sches.push($scope.sche); 
                    displayMsg('success', 'Successfull', $scope.sche.Count+ ' Message Scheduled Successfully');
                    $scope.sche = {AppUserId: userId, Message: "", Destination: "", CountryId: cityId}; 
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
            $scope.single = function(){
                $scope.sing.Type = "Broadcast Alert"; $scope.sing.Page = $scope.sing.SmsPrice;
                $http.post( Urls+"/Birthdays/",JSON.stringify($scope.sing),{headers: headers}).then(function(response){
                    $scope.reg = response.data; $scope.sing = {AppUserId: userId, CountryId: cityId};
                    displayMsg('success', 'Sent', "Message Sent Successfully"); 
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
            $scope.getnos = function(val) {
                // alert('getting');
                $scope.send.Option = val; var phone =''; //console.log(val);
                switch (val) {
                    case 'All':
                        {
                            angular.forEach($scope.custs, function (value, key){
                                if(value.Phone.length >= 9){ 
                                    phone += value.Phone+',';
                                }
                            });
                            angular.forEach($scope.staffs, function (value, key){
                                if(value.Phone.length >= 9){
                                    phone += value.Phone+',';
                                }
                            });
                            break;
                        };
                    case 'Staff':
                        {
                            angular.forEach($scope.staffs, function (value, key){
                                if(value.Phone.length >= 9){
                                    phone += value.Phone+',';
                                }
                            });
                            break;
                        };
                    case 'Customer':
                        {
                            angular.forEach($scope.custs, function (value, key){
                                if(value.Phone.length >= 9){
                                    phone += value.Phone+',';
                                }
                            });
                            break;
                        };
                    }
                $scope.send.Destination = phone; $scope.getcount(phone);
            };
            $scope.sgetnos = function(val) {
                // alert('getting');
                $scope.send.Option = val; var phone =''; //console.log(val);
                switch (val) {
                    case 'All':
                        {
                            angular.forEach($scope.custs, function (value, key){
                                if(value.Phone.length >= 9){ 
                                    phone += value.Phone+',';
                                }
                            });
                            angular.forEach($scope.staffs, function (value, key){
                                if(value.Phone.length >= 9){
                                    phone += value.Phone+',';
                                }
                            });
                            break;
                        };
                    case 'Staff':
                        {
                            angular.forEach($scope.staffs, function (value, key){
                                if(value.Phone.length >= 9){
                                    phone += value.Phone+',';
                                }
                            });
                            break;
                        };
                    case 'Customer':
                        {
                            angular.forEach($scope.custs, function (value, key){
                                if(value.Phone.length >= 9){
                                    phone += value.Phone+',';
                                }
                            });
                            break;
                        };
                    }
                $scope.sche.Destination = phone; $scope.getecount(phone);
            };
            $scope.getno = function() {
                //alert('getting');
                if ($scope.option == true){
                    $scope.sing.StaffId = '';
                    angular.forEach($scope.custs, function (value, key){
                        if($scope.sing.CustomerId == value.CustomerId){
                            $scope.sing.Destination = value.Phone;
                        }
                    });
                }else if($scope.option == false){
                    $scope.sing.CustomerId = '';
                    angular.forEach($scope.staffs, function (value, key){
                        if($scope.sing.StaffId == value.StaffId){
                            $scope.sing.Destination = value.Phone;
                        }
                    });
                }
            };
            $scope.savemsg = function() {
                $scope.save.Page = $scope.save.SmsPrice;
                $scope.save.AppUserId = userId; $scope.save.CountryId = cityId;
                $http.post( Urls+"/AppMessages/",JSON.stringify($scope.save),{headers: headers}).then(function(response){
                    $scope.save = response.data.Message; //console.log(response);
                    displayMsg('success', 'Send Successfully', response.data.Output); 
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
            $scope.getsave = function() {
                var type = $scope.save.Type;
                $http.get( Urls+"/AppMessages/"+type+'/'+userId,{headers: headers}).then(function(response){
                    $scope.reg = response.data; $scope.save = $scope.reg; $scope.sachange($scope.reg.Messages.length);
                    // displayMsg('success', 'Send Successfully', $scope.reg.Output); 
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
            $scope.change = function(value) {
                $scope.rpt = true;
                if(value == null){
                    $scope.send.SmsPrice = 1; $scope.send.Message = '';
                }else if(value <= 160){
                    $scope.send.SmsPrice = 1; $scope.maxlimit = 160;
                }else if(value <= 320){
                    $scope.send.SmsPrice = 2; $scope.maxlimit = 320;
                    $scope.send.Cost = $scope.send.SmsPrice * $scope.send.Count;
                }else if(value <= 480){
                    $scope.send.SmsPrice = 3; $scope.maxlimit = 480;
                    $scope.send.Cost = $scope.send.SmsPrice * $scope.send.Count;
                }else if(value <= 640){
                    $scope.send.SmsPrice = 4; $scope.maxlimit = 640;
                    $scope.send.Cost = $scope.send.SmsPrice * $scope.send.Count;
                }else{
                    $scope.send.SmsPrice = 5; $scope.maxlimit = 820;
                    $scope.send.Cost = $scope.send.SmsPrice * $scope.send.Count;
                }
                $scope.getcount($scope.send.Destination);
            };
            $scope.schange = function(value) {
                $scope.srpt = true;
                if(value == null){
                    $scope.sing.SmsPrice = 1; $scope.sing.Message = '';
                }else if(value <= 160){
                    $scope.sing.Cost = $scope.sing.SmsPrice = 1; $scope.maxlimit = 160;
                }else if(value <= 320){
                    $scope.sing.Cost = $scope.sing.SmsPrice = 2; $scope.maxlimit = 320;
                }else if(value <= 480){
                    $scope.sing.Cost = $scope.sing.SmsPrice = 3; $scope.maxlimit = 480;
                }else if(value <= 640){
                    $scope.sing.Cost = $scope.sing.SmsPrice = 4; $scope.maxlimit = 640;
                }else{
                    $scope.sing.Cost = $scope.sing.SmsPrice = 5; $scope.maxlimit = 820;
                }
            };
            $scope.echange = function(value) {
                $scope.erpt = true;
                if(value == null){
                    $scope.sche.SmsPrice = 1; $scope.sche.Message = '';
                }else if(value <= 160){
                    $scope.sche.SmsPrice = 1; $scope.maxlimit = 160;
                }else if(value <= 320){
                    $scope.sche.SmsPrice = 2; $scope.maxlimit = 320;
                    $scope.sche.Cost = $scope.sche.SmsPrice * $scope.sche.Count;
                }else if(value <= 480){
                    $scope.sche.SmsPrice = 3; $scope.maxlimit = 480;
                    $scope.sche.Cost = $scope.sche.SmsPrice * $scope.sche.Count;
                }else if(value <= 640){
                    $scope.sche.SmsPrice = 4; $scope.maxlimit = 640;
                    $scope.sche.Cost = $scope.sche.SmsPrice * $scope.sche.Count;
                }else{
                    $scope.sche.SmsPrice = 5; $scope.maxlimit = 820;
                    $scope.sche.Cost = $scope.sche.SmsPrice * $scope.sche.Count;
                }
                $scope.getecount($scope.sche.Destination);
            };
            $scope.sachange = function(value) {
                $scope.sarpt = true;
                if(value == null){
                    $scope.save.SmsPrice = 1; $scope.save.Message = '';
                }else if(value <= 160){
                    $scope.save.SmsPrice = 1; 
                }else if(value <= 320){
                    $scope.save.SmsPrice = 2; $scope.maxlimit = 320;
                    $scope.save.Cost = $scope.save.SmsPrice * $scope.save.Count;
                }else if(value <= 480){
                    $scope.save.SmsPrice = 3; $scope.maxlimit = 480;
                    $scope.save.Cost = $scope.save.SmsPrice * $scope.save.Count;
                }else if(value <= 640){
                    $scope.save.SmsPrice = 4; $scope.maxlimit = 640;
                    $scope.save.Cost = $scope.save.SmsPrice * $scope.save.Count;
                }else{
                    $scope.save.SmsPrice = 5; $scope.maxlimit = 820;
                    $scope.save.Cost = $scope.save.SmsPrice * $scope.save.Count;
                }
            };
            $scope.getcount = function(value) {
                $scope.dph = true;
                if (value != null) {
                    var no = value.split(','); $scope.send.Count = no.length; 
                    $scope.send.Cost = $scope.send.SmsPrice * $scope.send.Count;
                }else{ $scope.send.Count = 0; $scope.send.Cost = $scope.send.SmsPrice * $scope.send.Count}
            };
            $scope.getecount = function(value) {
                $scope.edph = true;
                if (value != null) {
                    var no = value.split(','); $scope.sche.Count = no.length; 
                    $scope.sche.Cost = $scope.sche.SmsPrice * $scope.sche.Count;
                }else{ $scope.sche.Count = 0; $scope.sche.Cost = $scope.sche.SmsPrice * $scope.sche.Count}
            };
        });

        myApp.controller('birthcontroller', function($scope, $http) {
            $http.get(Urls+"/Birthdays/User/"+userId, {headers: headers}).then(function (response) {
                $scope.birthday = response.data;
            }, function(response) { 
                displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                checkStatus(response.status);
            });
            $scope.count = limit; 
            $scope.load = function() {
                $scope.page++;
                $scope.count = limit * $scope.page; 
            };
            $scope.retry = function(id) {
                $http.get( Urls+"/Birthdays/Retry/"+id,{headers: headers}).then(function(response){
                    $scope.resp = response.data; $scope.staffs.push($scope.staff);
                    displayMsg('success', 'Successfully', "Message Resented Successfully");
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                });
            };
        });

        myApp.controller('smslogcontroller', function($scope, $http) {
            $http.get(Urls+"/AppLogs/User/"+userId, {headers: headers}).then(function (response) {
                $scope.smslog = response.data;
            }, function(response) { 
                displayMsg('warning', 'Not Successfull', response.statusText+': '+response.data);
                checkStatus(response.status);
            });
            $scope.count = limit; 
            $scope.load = function() {
                $scope.page++;
                $scope.count = limit * $scope.page; 
            };
            $scope.retry = function(id) {
                $http.get( Urls+"/AppLogs/Retry/"+id,{headers: headers}).then(function(response){
                    $scope.resp = response.data; //console.log(response);
                    displayMsg('success', 'Successfully', "Message Resented Successfully");
                }, function(response) { 
                    displayMsg('warning', 'Not Successfull', response.statusText);
                });
            };
        });

        function displayMsg(type, title, text){
            var type = type, title = title, text = text;
            swal({
                title: title,
                text: text,
                type: type
            });
        }

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [ day, month, year].join('-');
        }

        function getToken(){
            $.ajax({
                type: 'GET',
                async: false,
                url: Urls+'/AccessToken/',
                dataType: 'json',
                success: function(data){
                    auth = data.access_token;
                }
            });
          return auth;
        }

        function fileUpload(type, file){
            var name, fil = file; var seq = type; 
            var data = new FormData();
            console.log(seq); console.log(fil);
            if (fil.length > 0) {
               data.append("file", fil[0]);
            }
            $.ajax({
                type: "POST",
                url: Urls +"/Upload/"+ seq,
                contentType: false,
                processData: false,
                async: false,
                headers: {
                    'Access-Control-Allow-Origin' : '*',
                    'Authorization' : 'Bearer ' + auth
                },
                data: data,
                success: function(jqXHR) {
                    name = jqXHR;
                    //console.log(jqXHR);
                },
            });
          return name;
        }

        function checkStatus(status){
            switch (status) {
                case 401:
                    {
                        toastr.error('Invalid Access Token, You are Unauthorized to Access this Application','Unauthorized User');
                        sessionStorage.clear();
                        window.location.href = "login";
                        break;
                    };
                case 500:
                    {
                        console.log(status);
                        break;
                    };
                case 501:
                    {
                        toastr.error('This you are looking for doesnt exist in this Application','Not Found');
                        console.log(status);
                        break;
                    };
                case 404:
                    {
                        toastr.error('This you are looking for doesnt exist in this Application','Internal Server Error');
                        console.log(status);
                        break;
                    };
                case -1:
                    {
                        toastr.error('Unable To get Data, Please Check Your internet Connection','Connection Error');
                        break;
                    };
                }
        }

        function logOut(){ 
            $.ajax({
                type: 'GET',
                url: Urls +'/AppUsers/' + userId,
                dataType: 'json',
                headers: {
                    'Authorization' : 'Bearer ' + auth
                },
                success: function(data){
                    $.ajax({
                        type: 'POST',
                        url: Urls +'/Out/'+ data.Username,
                        dataType: 'json',
                        headers: {
                            'Authorization' : 'Bearer ' + auth
                        },
                        success: function (data) {
                            displayMsg("success", 'logOut Successfull', 'You have been Logout Successfully');
                            sessionStorage.clear();
                            window.location.href = "login";
                        },
                        error: function(jqXHR){
                            sessionStorage.clear();
                            window.location.href = "login";
                        }
                    }); 
                },
                error: function(jqXHR){
                    sessionStorage.clear();
                    window.location.href = "login";
                }
            });
        }
